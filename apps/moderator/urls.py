from django.conf.urls.defaults import *
from django.contrib.auth import views

# your_app = name of your root django app.
urlpatterns = patterns('openuniversity.apps.moderator.views',
    url(r'^$', 'home', name="moderator"),
    
    url(r'^content/subjects/$', 'subject_list', name="subject_list"),    
    url(r'^content/subjects/add/$', 'subject_add', name="subject_add"),
    url(r'^content/(?P<s_str>[\w\-]+)/$', 'subject_view'),
    url(r'^content/(?P<s_str>[\w\-]+)/edit/$', 'subject_add'),
    url(r'^content/(?P<s_str>[\w\-]+)/remove/$', 'subject_remove'),
    url(r'^content/(?P<s_str>[\w\-]+)/translate/$', 'subject_translate'),
    
    url(r'^content/(?P<s_str>[\w\-]+)/topics/$', 'topic_list'),
    url(r'^content/(?P<s_str>[\w\-]+)/topics/add/$', 'topic_add'),
    url(r'^content/(?P<s_str>[\w\-]+)/(?P<t_str>[\w\-]+)/$', 'topic_view'),
    url(r'^content/(?P<s_str>[\w\-]+)/(?P<t_str>[\w\-]+)/edit/$', 'topic_add'),
    url(r'^content/(?P<s_str>[\w\-]+)/(?P<t_str>[\w\-]+)/remove/$', 'topic_remove'),
    url(r'^content/(?P<s_str>[\w\-]+)/(?P<t_str>[\w\-]+)/translate/$', 'topic_translate'),

    url(r'^content/(?P<s_str>[\w\-]+)/(?P<t_str>[\w\-]+)/lessons/$', "lesson_list"),
    url(r'^content/(?P<s_str>[\w\-]+)/(?P<t_str>[\w\-]+)/lessons/add/$', "lesson_add"),
    url(r'^content/(?P<s_str>[\w\-]+)/(?P<t_str>[\w\-]+)/(?P<l_str>[\w\-]+)/$', 'lesson_view'),
    url(r'^content/(?P<s_str>[\w\-]+)/(?P<t_str>[\w\-]+)/(?P<l_str>[\w\-]+)/edit/$', "lesson_add"),
    url(r'^content/(?P<s_str>[\w\-]+)/(?P<t_str>[\w\-]+)/(?P<l_str>[\w\-]+)/remove/$', 'lesson_remove'),
    url(r'^content/(?P<s_str>[\w\-]+)/(?P<t_str>[\w\-]+)/(?P<l_str>[\w\-]+)/translate/$', 'lesson_translate'),
    url(r'^content/(?P<s_str>[\w\-]+)/(?P<t_str>[\w\-]+)/(?P<l_str>[\w\-]+)/set-thumb/$', 'lesson_set_thumbnail'),
    
    url(r'^content/(?P<s_str>[\w\-]+)/(?P<t_str>[\w\-]+)/(?P<l_str>[\w\-]+)/exercises/$', 'exercise_list'),
    url(r'^content/(?P<s_str>[\w\-]+)/(?P<t_str>[\w\-]+)/(?P<l_str>[\w\-]+)/exercises/add$', 'exercise_add'),
    url(r'^content/(?P<s_str>[\w\-]+)/(?P<t_str>[\w\-]+)/(?P<l_str>[\w\-]+)/(?P<e_str>[\w\-]+)/$', 'exercise_view'),
    url(r'^content/(?P<s_str>[\w\-]+)/(?P<t_str>[\w\-]+)/(?P<l_str>[\w\-]+)/(?P<e_str>[\w\-]+)/edit/$', 'exercise_add'),
    url(r'^content/(?P<s_str>[\w\-]+)/(?P<t_str>[\w\-]+)/(?P<l_str>[\w\-]+)/(?P<e_str>[\w\-]+)/remove/$', 'exercise_remove'),
    url(r'^content/(?P<s_str>[\w\-]+)/(?P<t_str>[\w\-]+)/(?P<l_str>[\w\-]+)/(?P<e_str>[\w\-]+)/translate/$', 'exercise_translate'),
    
    url(r'^content/(?P<s_str>[\w\-]+)/(?P<t_str>[\w\-]+)/(?P<l_str>[\w\-]+)/(?P<e_str>[\w\-]+)/questions/$', 'exercise_question_list'),
    url(r'^content/(?P<s_str>[\w\-]+)/(?P<t_str>[\w\-]+)/(?P<l_str>[\w\-]+)/(?P<e_str>[\w\-]+)/questions/add/$', 'exercise_question_add'),
    url(r'^content/(?P<s_str>[\w\-]+)/(?P<t_str>[\w\-]+)/(?P<l_str>[\w\-]+)/(?P<e_str>[\w\-]+)/(?P<q_str>[\w\-]+)/$', 'exercise_question_view'),
    url(r'^content/(?P<s_str>[\w\-]+)/(?P<t_str>[\w\-]+)/(?P<l_str>[\w\-]+)/(?P<e_str>[\w\-]+)/(?P<q_id>\d+)/edit/$', 'exercise_question_add'),
    url(r'^content/(?P<s_str>[\w\-]+)/(?P<t_str>[\w\-]+)/(?P<l_str>[\w\-]+)/(?P<e_str>[\w\-]+)/(?P<q_id>\d+)/remove/$', 'exercise_question_remove'),
    url(r'^content/(?P<s_str>[\w\-]+)/(?P<t_str>[\w\-]+)/(?P<l_str>[\w\-]+)/(?P<e_str>[\w\-]+)/(?P<q_id>\d+)/translate/$', 'exercise_question_translate'),

    url(r'^suggested-questions/$','suggested_question_list'),
    url(r'^suggested-questions/edit/(?P<id>\d+)/$','edit_suggested_question'),
    url(r'^suggested-questions/remove/(?P<id>\d+)/$','remove_suggested_question'),

)