#coding: utf-8
'''
Created on Sep 26, 2011

@author: javkhlan
'''
from django import forms
from django.db.models.query_utils import Q
from django.forms.models import ModelForm
from openuniversity.apps.core.models import Language
from openuniversity.apps.translation.models import TranslateAble, Translation

class TranslateAbleForm(ModelForm):
    def __init__(self, *args, **kwargs):
        print "TranslateAbleForm.__init__"
        print kwargs
        from django.utils import translation
        """
            local_language is passed in translate forms and edit forms,
            if local_language i used, language is not selected
        """
        if kwargs.has_key('instance') and hasattr(kwargs['instance'], 'translations'): # this is called in edit function
            if (kwargs.has_key("local_language")):
                translations = kwargs['instance'].translations.filter(language=Language.objects.get(code=kwargs['local_language']))
                self.base_fields['language'].initial = Language.objects.get(code=kwargs['local_language'])
                self.base_fields['language'].widget = forms.HiddenInput()
#                print "1"
            else:
                translations = kwargs['instance'].translations.filter(language__code="en-us")
#                print "2"
            if translations.count():
                for translation in translations.all():
                    self.base_fields[translation.field_name].initial = translation.text
                self.base_fields['language'].initial = translation.language.id
#                print "3"
            else:
                for field_name in kwargs['instance'].get_trans_field_names():
                    self.base_fields[field_name].initial = ""
#                    print "4"
                if kwargs.has_key('local_language'):
                    self.base_fields['language'].initial = Language.objects.get(code=kwargs['local_language']).id
#                    print "5"
                else:
                    self.base_fields['language'] = forms.ModelChoiceField(queryset=Language.objects.all())
#                    print "6"
        else: # this is called in add function and in models that aren't translatable
            for bf_key in self.base_fields.keys():
                self.base_fields[bf_key].initial = ""
                self.base_fields['language'] = forms.ModelChoiceField(queryset=Language.objects.all())
#                print "7"

        if kwargs.has_key("auto_id"): # temporary 
            del(self.base_fields['language'])
#            print "8"
        
        if kwargs.has_key("local_language"):
            del(kwargs['local_language'])
#            print "9"
        super(TranslateAbleForm, self).__init__(*args, **kwargs)

    class Meta:
        model = TranslateAble
    
    def save(self, *args, **kwargs):
        print "TranslateableForm.save()", args, kwargs
        if kwargs.has_key('add_info') and kwargs['add_info'].has_key('language'):
            language = Language.objects.get(pk=int(kwargs['add_info']['language']))
            kwargs.pop('add_info')
        instance = super(ModelForm, self).save(**kwargs)
        
        for field_name in instance.get_trans_field_names():
            t = Translation()
            if self.cleaned_data.has_key("language"):
                if instance.translations.filter(Q(field_name=field_name)&Q(language=self.cleaned_data['language'])).count():
                    t = instance.translations.get(Q(field_name=field_name)&Q(language=self.cleaned_data['language']))
                else:
                    t.object = instance
                t.field_name = field_name
                t.text = self.cleaned_data[field_name]
                t.language = self.cleaned_data['language']
                t.save()
            else:
                if instance.translations.filter(Q(field_name=field_name)&Q(language=language)).count():
                    t = instance.translations.get(Q(field_name=field_name)&Q(language=language))
                else:
                    t.object = instance
                t.field_name = field_name
                t.text = self.cleaned_data[field_name]
                t.language = language
                t.save()
                
        return instance

class AutoCurrentLanguageTranslateAbleForm(ModelForm):
    def __init__(self, *args, **kwargs):
        from django.utils import translation
        if kwargs.has_key('instance') and \
        hasattr(kwargs['instance'], 'translations'): # this is called in edit function
            translations = kwargs['instance'].translations.filter(language=
                      Language.objects.get(code=translation.get_language()))
        
            if translations.count():
                for translation in translations.all():
                    self.base_fields[translation.field_name].initial = translation.text
                    print "3"
            else:
                for field_name in kwargs['instance'].get_trans_field_names():
                    self.base_fields[field_name].initial = ""
                    print "4"
        else:
            for bf_key in self.base_fields.keys():
                self.base_fields[bf_key].initial = ""

        super(AutoCurrentLanguageTranslateAbleForm, self).__init__(*args, **kwargs)

    class Meta:
        model = TranslateAble
    
    def save(self, *args, **kwargs):
        from django.utils import translation
        instance = super(ModelForm, self).save(**kwargs)
        language = Language.objects.get(code=translation.get_language())
        
        for field_name in instance.get_trans_field_names():
            t = Translation()
            if instance.translations.filter(Q(field_name=field_name)&Q(language=language)).count():
                t = instance.translations.get(Q(field_name=field_name)&Q(language=language))
            else:
                t.object = instance
            t.field_name = field_name
            t.text = self.cleaned_data[field_name]
            t.language = Language.objects.get(code=translation.get_language())
            t.save()
                
        return instance

class TranslationForm(ModelForm):
    class Meta:
        model = Translation