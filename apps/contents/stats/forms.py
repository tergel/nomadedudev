#coding: utf-8
'''
Created on Sep 26, 2011

@author: javkhlan
'''
from django import forms
from openuniversity.apps.contents.stats.models import Account_Lesson_Question, \
    Account_Lesson, CompletedExercise

class Account_Topic_QuestionForm(forms.ModelForm):
    class Meta:
        model = Account_Lesson_Question

class Account_LessonForm(forms.ModelForm):
    class Meta:
        model = Account_Lesson

class CompletedLessonExerciseForm(forms.ModelForm):
    class Meta:
        model = CompletedExercise