from django.db import models
from openuniversity.apps.translation.models import TranslateAble
from openuniversity.apps.core.models import Account
from openuniversity.apps.common.models import SubType

# Create your models here.
class Course(TranslateAble):
    contents = models.ManyToManyField(TranslateAble, related_name="courses",
                  through="CourseContents", null=True, blank=True)
    created_account = models.ForeignKey(Account, related_name="courses")
    course_type = models.ForeignKey(SubType)
    started_date = models.DateTimeField(auto_now_add=True)
    edited_date = models.DateTimeField(auto_now=True)
    is_published = models.BooleanField(default=True)
    
    def get_trans_field_names(self):
        return ("name", "description")
    def __unicode__(self):
        return self.t9n(field_name="name", current_language=True, 
                        show_language=True)

class CourseContents(models.Model):
    course = models.ForeignKey(Course, related_name="course_contents")
    content = models.ForeignKey(TranslateAble)
    added_account = models.ForeignKey(Account, null=True)
    content_level = models.ForeignKey(SubType, related_name="course_contents")