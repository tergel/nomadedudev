#coding: utf-8
'''
Created on Sep 27, 2011

@author: javkhlan
'''
from django.contrib import admin
from openuniversity.apps.common.forms import SubTypeForm, DifficultyForm
from openuniversity.apps.common.models import SubType, Difficulty

class SubTypeAdmin(admin.ModelAdmin):
    form = SubTypeForm
admin.site.register(SubType, SubTypeAdmin)

class DifficultyAdmin(admin.ModelAdmin):
    form = DifficultyForm
admin.site.register(Difficulty, DifficultyAdmin)